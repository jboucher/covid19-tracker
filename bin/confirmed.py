from __future__ import print_function

import csv
import sys
import urllib2
from datetime import datetime

try:
    url = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_19-covid-Confirmed.csv'
    response = urllib2.urlopen(url)
    cr = csv.reader(response)

    for line in cr:
        print(str(datetime.now()) + ";" + line[0] + ";" + line[1] + ";" + line[2] + ";" + line[3] + ";" + line[-1])

except IOError, e:
    if hasattr(e, 'code'):
        sys.stderr.write("Error occurred when attempting to retrieve data: " + str(e.code))
    elif hasattr(e, 'reason'):
        sys.stderr.write("Error occurred when attempting to retrieve data: " + str(e.reason))
